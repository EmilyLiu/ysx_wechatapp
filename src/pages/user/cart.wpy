<template>
  <view class="cart-container">
    <view class="goods-list">
      <block wx:for="{{cartList}}" wx:key="{{index}}">
        <scroll-view 
          scroll-x 
          scroll-with-animation
          @scroll="handleScroll" 
          data-index="{{index}}" 
          scroll-left="{{item.scrollLeft}}">
          <view class="flex-row goods-scroll-wrap">
            <view class="flex-row list-cell">
              <view @tap.stop="handleGoodsSelect({{index}})">
                <image wx:if="{{!item.selected}}" src="/images/bt_select_nor.png" class="select-icon"/>
                <image wx:else src="/images/bt_select_sel.png" class="select-icon"/>
              </view>
              <image class="goods-pic" src="{{item.firstPicture}}"/>
              <view class="flex-column goods-detail">
                <view class="flex-column">
                  <text class="goods-name">{{item.goodsName}}</text>
                  <view class="flex-row good-note">
                    <text class="goods-specifications">规格：{{item.unitName}}</text>
                    <text class="goods-time">预计8月2号送达</text>
                  </view>
                </view>
                <view class="flex-row goods-bottom">
                  <view class="flex-row goods-price">
                    <text class="pay-num">¥{{item.rolePrice}}</text>
                    <text class="origi-num" wx:if="{{item.rolePrice < item.goodsPrice}}">¥{{item.goodsPrice}}</text>
                  </view>
                  <view class="flex-row goods-opera">
                    <image src="/images/bt_subtract_nor.png" class="goods-number-btn" 
                      @tap.stop="subtractGoods({{index}})"/>
                    <text class="goods-number">{{item.goodsQuantity}}</text>
                    <image src="/images/bt_add_nor.png" class="goods-number-btn" 
                      @tap.stop="addGoods({{index}})"/>
                  </view>
                </view>
              </view>
            </view>
          <view class="goods-del" @tap.stop="handleDelete({{index}})">
            <image src="/images/pop_close_bt.png" class="del-icon"/>
          </view>
          </view>
        </scroll-view>
      </block>
    </view>
    <view class="flex-row cart-bottom">
      <view class="flex-column selecte-all">
        <view @tap="handleSelectedAll">
          <image wx:if="{{!seletedAll}}" src="/images/bt_select_nor.png" class="select-icon"/>
          <image wx:else src="/images/bt_select_sel.png" class="select-icon"/>
        </view>
        <text class="all-text">全选</text>
      </view>
      <view class="flex-column pay-content">
        <text class="pay-amount-text">合计</text>
        <text class="pay-num">¥ {{payPrice}}</text>
        <text class="pay-detail">总额：¥{{payAmount}} 优惠：¥{{payDiscount}}</text>
      </view>
      <view class="flex-row pay-btn" @tap="toTopayPage()">
        <text class="btn-text">立即支付</text>
      </view>
    </view>
  </view>
</template>
<script>
import wepy from 'wepy'
import { getCartList, modifyCart, deleteCart } from '../../network/api'
import error from './../../utils/error.js'
export default class extends wepy.page {
  config = {
    navigationBarTitleText: '购物车',
    backgroundColor: '#f6f6f6'
  }
  data = {
    cartList: [],
    seletedAll: false,
    payPrice: 0,
    payAmount: 0,
    payDiscount: 0,
    operaIndex: null,
    preScrollLeft: 0
  }
  methods = {
    handleScroll(e) {
      let index = e.currentTarget.dataset.index
      let scrollLeft = e.detail.scrollLeft
      if (this.operaIndex === null) {
        this.operaIndex = index
        this.preScrollLeft = scrollLeft
      } else if (this.operaIndex === index) {
        let offset = this.preScrollLeft - scrollLeft
        if (offset > 0) {
          this.cartList[index].scrollLeft = 0
          this.preScrollLeft = 0
        } else if (offset < 0) {
          this.cartList[index].scrollLeft = 62
          this.preScrollLeft = 62
        }
      } else {
        this.preScrollLeft = scrollLeft
        this.operaIndex = index
      }
    },
    handleDelete(index) {
      let userId = wepy.getStorageSync('userId')
      let cartIds = [this.cartList[index].cartId]
      deleteCart(cartIds, userId).then(res => {
        if (res.code === 200) {
          wepy.showToast({
            title: '删除成功',
            icon: 'success'
          })
          this.getCartList()
        } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
          error.reLogin()
        } else {
          error.tipsError(res.msg)
        }
      })
    },
    toTopayPage() {
      let orderDetails = {
        list: this.cartList.filter(item => item.selected),
        payPrice: this.payPrice,
        payAmount: this.payAmount,
        payDiscount: this.payDiscount
      }
      if (orderDetails.list.length === 0) {
        wepy.showToast({
          title: '请选择商品',
          icon: 'none'
        })
        return
      }
      wepy.setStorageSync('orderDetails', orderDetails)
      wx.navigateTo({url: 'order-topay'})
    },
    handleGoodsSelect(index) {
      this.cartList[index].selected = !this.cartList[index].selected
      if (!this.cartList[index].selected) this.seletedAll = false
      this.computePrice()
    },
    handleSelectedAll() {
      this.seletedAll = !this.seletedAll
      this.cartList.forEach(item => {
        item.selected = this.seletedAll
      })
      this.computePrice()
    },
    subtractGoods(index) {
      let goods = this.cartList[index]
      if (goods.goodsQuantity === 1) {
        wepy.showToast({
          title: '不能减少了哟~',
          icon: 'none'
        })
        return
      }
      goods.goodsQuantity = goods.goodsQuantity - 1
      let params = {
        'cartId': goods.cartId,
        'goodsQuantity': goods.goodsQuantity
      }
      modifyCart(params).then(res => {
        if (res.code === 200) {
          if (goods.selected) {
            this.computePrice()
            this.$apply()
          }
        } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
          error.reLogin()
        } else {
          error.tipsError(res.msg)
        }
      })
    },
    addGoods(index) {
      let goods = this.cartList[index]
      // 没有限购字段？？
      goods.goodsQuantity = goods.goodsQuantity + 1
      let params = {
        'cartId': goods.cartId,
        'goodsQuantity': goods.goodsQuantity
      }
      modifyCart(params).then(res => {
        if (res.code === 200) {
          if (goods.selected) {
            this.computePrice()
            this.$apply()
          }
        } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
          error.reLogin()
        } else {
          error.tipsError(res.msg)
        }
      })
    }
  }
  computePrice() {
    this.payPrice = 0
    this.payAmount = 0
    this.payDiscount = 0
    this.cartList.forEach((item) => {
      if (item.selected) {
        this.payPrice += item.rolePrice * item.goodsQuantity
        this.payAmount += item.goodsPrice * item.goodsQuantity
      }
    })
    this.payPrice = Number(this.payPrice).toFixed(2)
    this.payAmount = Number(this.payAmount).toFixed(2)
    this.payDiscount = (this.payAmount - this.payPrice).toFixed(2)
  }
  getCartList() {
    let userId = wepy.getStorageSync('userId')
    let params = {
      'userId': userId
    }
    getCartList(params).then(res => {
      if (res.code === 200) {
        this.cartList = res.data.list
        this.cartList.forEach(item => {
          item.selected = false
          item.scrollLeft = 0
        })
        this.$apply()
      } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
        error.reLogin()
      } else {
        error.tipsError(res.msg)
      }
    })
  }
  init() {
    this.seletedAll = false
    this.payPrice = 0
    this.payAmount = 0
    this.payDiscount = 0
    this.operaIndex = null
    this.preScrollLeft = 0
  }
  onShow() {
    this.init()
    this.getCartList()
  }
}
</script>
<style lang="less">
page{
  background-color: #f6f6f6;
}
.cart-container{
  // padding: 60rpx 32rpx;
  font-family: PingFang SC;
  padding: 60rpx 32rpx 236rpx 32rpx;
}
.select-icon{
  width: 80rpx;
  height: 80rpx;
}
.goods-scroll-wrap{
  align-items: center;
}
.goods-list{
  .list-cell{
    width: 686rpx;
    box-sizing: border-box;
    padding: 0 16rpx;
    height: 236rpx;
    background-color: #fff;
    border-radius: 20rpx;
    box-shadow: 0rpx 6rpx 15rpx rgba(0,0,0,0.05);
    margin-bottom: 20rpx;
    align-items: center;
    justify-content: space-between;
    .goods-pic{
      width: 180rpx;
      height: 180rpx;
      background-color: gray;
    }
    .goods-detail{
      width: 400rpx;
      height: 180rpx;
      padding: 0 24rpx;
      justify-content: space-between;
      .goods-name{
        font-size: 28rpx;
        color: #111;
        line-height: 40rpx;
        padding-bottom: 10rpx;
      }
      .good-note{
        justify-content: space-between;
        font-size: 22rpx;
        .goods-specifications{
          color: #999;
        }
        .goods-time{
          color: #20c442;
        }
      }
      .goods-bottom{
        justify-content: space-between;
        .goods-price{
          align-items: flex-end;
          font-size: 36rpx;
          color: #111;
          .origi-num{
            color: #666;
            font-size: 24rpx;
            padding-bottom: 6rpx;
            text-decoration: line-through;
            margin-left: 10rpx;
          }
        }
        .goods-number{
          font-size: 28rpx;
          color: #666;
          padding: 0 10rpx;
        }
      }
      .goods-opera{
        align-items: center;
        .goods-number-btn{
          width: 48rpx;
          height: 48rpx;
        }
      }
    }
  }
}
.goods-del{
  .del-icon{
    width: 56rpx;
    height: 56rpx;
    margin: 0 35rpx;
  }
}
.cart-bottom{
  width: 750rpx;
  height: 200rpx;
  box-sizing: border-box;
  border-top-left-radius: 40rpx;
  border-top-right-radius: 40rpx;
  background-color: #fff;
  box-shadow: 0 -20rpx 40rpx rgba(214,220,228,0.4);
  position: fixed;
  bottom: 10rpx;
  left: 0;
  justify-content: space-between;
  align-items: center;
  padding: 0 36rpx;
  .selecte-all{
    align-items: center;
    .all-text{
      font-size: 28rpx;
      color: #111;
    }
  }
  .pay-content{
    .pay-amount-text{
      color: #111;
      font-size: 20rpx;
    }
    .pay-num{
      color: #011706;
      font-size: 48rpx;
      font-weight: bold;
    }
    .pay-detail{
      color: #727C8E;
      font-size: 24rpx;
    }
  }
  .pay-btn{
    width: 300rpx;
    color: #fff;
    height: 100rpx;
    border-radius: 100rpx;
    background-color: #20c442;
    box-shadow: 4rpx 8rpx 20rpx rgba(32, 196, 66, 0.5);
    align-items: center;
    justify-content: center;
    .btn-text{
      font-size: 32rpx;
    }
  }
}
</style>
