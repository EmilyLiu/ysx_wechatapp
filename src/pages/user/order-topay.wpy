<template>
  <view class="topay-container">
    <view class="flex-row white-box topay-address" wx:if="{{orderInfo.sendType === '团长'}}" @tap="bindLeaderChoose">
      <view class="flex-column address-content">
        <view class="flex-row receiver-name">
          <view class="receiver-icon">
            <text>团长</text>
          </view>
          <text class="name-text">{{leaderInfo.groupLeaderName}}</text>
        </view>
        <view class="flex-column address-selected">
          <text class="address-community">社群：{{leaderInfo.groupName}}</text>
          <text class="address-text">{{leaderInfo.groupAddress}} {{leaderInfo.detailAddress}}</text>
        </view>
      </view>
      <image src="/images/arrow_right_thin.png" class="arrow-right-icon"/>
    </view>
    <view class="flex-row white-box topay-address" wx:if="{{orderInfo.sendType === '快递'}}"  @tap="bindaddressChoose">
      <view class="flex-column address-content" wx:if="{{addressInfo}}">
        <view class="flex-row receiver-name">
          <view class="receiver-icon">
            <text>快递</text>
          </view>
          <text class="name-text">{{addressInfo.consigneeName}}</text>
        </view>
        <view class="flex-column address-selected">
          <text class="address-text">{{addressInfo.consigneePhoneNumber}}</text>
          <text class="address-text">{{addressInfo.addressString}}</text>
        </view>
      </view>
      <view class="flex-column address-content" wx:if="{{!addressInfo}}">
        <view class="flex-row receiver-name">
          <text class="name-text">请选择收货地址</text>
        </view>
      </view>
      <image src="/images/arrow_right_thin.png" class="arrow-right-icon"/>
    </view>
    <view class="white-box topay-goods">
      <block wx:for="{{orderDetails.list}}" wx:key="{{index}}">
        <view class="flex-row goods-cell">
          <image class="cell-img" src="{{item.firstPicture}}"/>
          <view class="flex-column cell-content">
            <view class="flex-column">
              <text class="goods-name">{{item.goodsName}}</text>
              <text class="goods-specifications">规格：{{item.unitName}}</text>
            </view>
            <view class="flex-row goods-bottom">
              <view class="flex-row goods-price">
                <text class="pay-num">￥{{item.rolePrice}}</text>
                <text class="origi-num" wx:if="{{item.rolePrice < item.goodsPrice}}">￥{{item.goodsPrice}}</text>
              </view>
              <view class="flex-row goods-opera">
                <image src="/images/bt_subtract_nor.png" class="goods-number-btn" 
                  @tap.stop="subtractGoods({{index}})"/>
                <text class="goods-number">{{item.goodsQuantity}}</text>
                <image src="/images/bt_add_nor.png" class="goods-number-btn" 
                  @tap.stop="addGoods({{index}})"/>
              </view>
            </view>
          </view>
        </view>
      </block>
    </view>
    <view class="white-box topay-delivery">
      <radio-group @change="deliveryChange">
        <label class="delivery-radio">
          <radio color="#20c442" value="团长" checked="{{true}}"/>送至团长
        </label>
        <label class="delivery-radio">
          <radio color="#20c442" value="快递"/>普通快递
        </label>
      </radio-group>
    </view>
    <view class="white-box topay-discont">
      <!-- <view class="flex-row discount-cell">
        <view class="cell-left">
          <text class="diacount-title">社区活动</text>
        </view>
        <view class="flex-row cell-right">
          <text class="discount-content">-4.00元</text>
          <image src="/images/arrow_right_grey.png" class="arrow-right-icon"/>
        </view>
      </view> -->
      <view class="flex-row discount-cell" @tap="toCouponSelectPage()">
        <view class="cell-left">
          <text class="diacount-title">优惠券</text>
        </view>
        <view class="flex-row cell-right" >
          <text class="discount-content" wx:if="{{couponList.length > 0 && !discountCoupon.discountCouponName}}">{{couponList.length}}张可用</text>
          <text class="discount-content" wx:if="{{couponList.length === 0}}">暂无可用优惠券</text>
          <text class="discount-content" style="color:#FF6F14" wx:if="{{couponList.length > 0 && discountCoupon}}">{{discountCoupon.discountCouponName}}</text>
          <image src="/images/arrow_right_grey.png" class="arrow-right-icon"/>
        </view>
      </view>
    </view>
    <view class="bottom-space"></view>
    <view class="white-box flex-row topay-bottom">
      <view class="flex-column bottom-left">
        <text class="total-text">合计</text>
        <text class="pay-amount">¥ {{orderDetails.payPrice}}</text>
        <text class="pay-detail">总额：¥{{orderDetails.payAmount}} 优惠：¥{{orderDetails.payDiscount}}</text>
      </view>
      <view class="topay-btn" @tap="createOrder">
        <text>立即支付</text>
      </view>
    </view>
  </view>
</template>

<script>
import wepy from 'wepy'
import { modifyCart, getDefaultAddress, getLeaderInfo, createOrder, getCouponList, getMostCoupon, payment } from '../../network/api'
import error from './../../utils/error'
export default class OrderTopay extends wepy.page {
  config = {
    navigationBarTitleText: '订单确认',
    backgroundColor: '#f6f6f6'
  }
  data = {
    orderDetails: null,
    addressInfo: null,
    leaderInfo: {},
    couponList: [],
    discountCoupon: {},
    orderInfo: {
      warehouseId: '',
      userDiscountCouponId: '',
      sendType: '团长',
      groupLeaderInfoId: '',
      userDeliveryAddressId: '',
      orderDetailVoList: []
    }
  }
  watch ={
    discountCoupon(newValue, oldValue) {
      let totalPrice = this.orderDetails.payPrice - this.discountCoupon.couponMoney
      if (totalPrice > 0) {
        this.orderDetails.payPrice = totalPrice
        this.orderDetails.payDiscount = this.orderDetails.payDiscount + this.discountCoupon.couponMoney
      } else {
        this.orderDetails.payDiscount = this.orderDetails.payAmount
        this.orderDetails.payPrice = 0
      }
    }
  }
  onLoad() {
    this.orderDetails = wepy.getStorageSync('orderDetails')
    this.orderInfo.warehouseId = wepy.getStorageSync('warehouseId')
    this.orderInfo.groupLeaderInfoId = wepy.getStorageSync('groupLeaderInfoId')
    this.orderInfo.groupLeaderInfoId = wepy.getStorageSync('groupLeaderInfoId')
    this.discountCoupon = wepy.getStorageSync('discountCoupon')
    let addressInfo = wepy.getStorageSync('address')
    if (addressInfo) {
      this.addressInfo = addressInfo
    } else {
      this.getDefaultAddress()
    }
    if (this.discountCoupon) {
      this.orderInfo.userDiscountCouponId = this.discountCoupon.userDiscountCouponId
    }
    this.getLeaderInfo()
    this.getMostCoupon()
  }
  onShow () {
    let addressInfo = wepy.getStorageSync('address')
    if (addressInfo) {
      this.addressInfo = addressInfo
    } else {
      this.getDefaultAddress()
    }
    this.discountCoupon = wepy.getStorageSync('discountCoupon')
    if (this.discountCoupon) {
      this.orderInfo.userDiscountCouponId = this.discountCoupon.userDiscountCouponId
    }
  }
  methods = {
    // 选择团长
    bindLeaderChoose () {
      wepy.navigateTo({ url: '/pages/user/select-leader?from=order' })
    },
    // 创建订单
    createOrder () {
      let _this = this
      let { orderInfo, orderDetails, addressInfo } = _this
      orderDetails.list.map(item => {
        let obj = {}
        obj.goodsId = item.goodsId
        obj.goodsQuantity = item.goodsQuantity
        orderInfo.orderDetailVoList.push(obj)
      })
      if (orderInfo.sendType !== '团长') {
        orderInfo.userDeliveryAddressId = addressInfo.userDeliveryAddressId
      }
      createOrder(orderInfo).then(res => {
        if (res.code === 200) {
          wepy.showToast({
            title: '获取支付信息中', // 提示的内容,
            icon: 'loading', // 图标,
            duration: 1000000, // 延迟时间,
            mask: true // 显示透明蒙层，防止触摸穿透,
          })
          wepy.login({
            success: data => {
              let params = {
                code: data.code,
                orderId: res.data.orderId
              }
              payment(params).then(result => {
                let payInfo = result.data
                wepy.requestPayment({
                  timeStamp: payInfo.timeStamp, // 时间戳从1970年1月1日00:00:00至今的秒数,即当前的时间,
                  nonceStr: payInfo.nonceStr, // 随机字符串，长度为32个字符以下,
                  package: payInfo.package, // 统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=*,
                  signType: 'MD5', // 签名算法，暂支持 MD5,
                  paySign: payInfo.paySign, // 签名,具体签名方案参见小程序支付接口文档,
                  success: payment => {
                    wepy.hideToast()
                    wepy.clearStorageSync('discountCoupon')
                    // 查看详情
                    wepy.redirectTo({url: 'order-detail?id=' + res.data.orderId + '&orderType=' + res.data.orderId})
                  },
                  fail: () => {
                  },
                  complete: () => {}
                })
              })
            },
            fail: () => {},
            complete: () => {}
          })
        } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
          error.reLogin()
        } else {
          error.tipsError(res.msg)
        }
      })
    },
    // 选择收货地址
    bindaddressChoose () {
      wepy.navigateTo({ url: 'address?from=order' })
    },
    // 选择优惠券
    toCouponSelectPage() {
      if (this.couponList.length > 0) {
        wepy.navigateTo({url: 'coupon?from=order&price=' + this.orderDetails.payAmount})
      }
    },
    subtractGoods(index) {
      let goods = this.orderDetails.list[index]
      if (goods.goodsQuantity === 1) {
        wepy.showToast({
          title: '不能减少了哟~',
          icon: 'none'
        })
        return
      }
      goods.goodsQuantity = goods.goodsQuantity - 1
      if (goods.cartId) {
        let params = {
          'cartId': goods.cartId,
          'goodsQuantity': goods.goodsQuantity
        }
        modifyCart(params).then(res => {
          if (res.code === 200) {
            if (goods.selected) {
              this.computePrice()
              this.$apply()
            }
          } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
            error.reLogin()
          } else {
            error.tipsError(res.msg)
          }
        })
      } else {
        this.computePrice()
      }
    },
    addGoods(index) {
      let goods = this.orderDetails.list[index]
      // 没有限购字段？？
      goods.goodsQuantity = goods.goodsQuantity + 1
      if (goods.cartId) {
        let params = {
          'cartId': goods.cartId,
          'goodsQuantity': goods.goodsQuantity
        }
        modifyCart(params).then(res => {
          if (res.code === 200) {
            if (goods.selected) {
              this.computePrice()
              this.$apply()
            }
          } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
            error.reLogin()
          } else {
            error.tipsError(res.msg)
          }
        })
      } else {
        this.computePrice()
      }
    }
  }
  // 获取可用优惠券
  getMostCoupon () {
    let _this = this
    let orderDetails = this.orderDetails
    let goodsList = []
    orderDetails.list.map(item => {
      let obj = {}
      obj.goodsId = item.goodsId
      obj.goodsPrice = item.rolePrice
      obj.goodsQuantity = item.goodsQuantity
      goodsList.push(obj)
    })
    let params = {
      goodInfoList: goodsList
    }
    // let paramsData = {
    //   pageNum: 1,
    //   pageSize: 1000000,
    //   isUse: false
    // }
    // getCouponList(paramsData).then(res => {
    //   if (res.code === 200) {
    //     _this.couponList = res.data.list
    //     _this.$apply()
    //   } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
    //     error.reLogin()
    //   } else {
    //     error.tipsError(res.msg)
    //   }
    // })
    getMostCoupon(params).then(res => {
      if (res.code === 200) {
        _this.couponList = res.data.list
        _this.$apply()
      } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
        error.reLogin()
      } else {
        error.tipsError(res.msg)
      }
    })
  }
  // 快递选择
  deliveryChange(e) {
    this.orderInfo.sendType = e.detail.value
  }
  // 查询默认地址
  getDefaultAddress () {
    let _this = this
    getDefaultAddress().then(res => {
      if (res.code === 200) {
        if (res.data) {
          _this.addressInfo = res.data
          _this.addressInfo.addressString = res.data.administrativeDivisionNames.split(',').join('') + res.data.detailAddress
          _this.$apply()
        }
      } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
        error.reLogin()
      } else {
        error.tipsError(res.msg)
      }
    })
  }
  // 获取团长信息
  getLeaderInfo () {
    let _this = this
    let groupLeaderInfoId = wepy.getStorageSync('groupLeaderInfoId')
    getLeaderInfo(groupLeaderInfoId).then(res => {
      _this.leaderInfo = res.data
    })
  }
  computePrice() {
    console.log(123)
    this.orderDetails.payAmount = 0
    this.orderDetails.payPrice = 0
    this.orderDetails.list.forEach((item) => {
      this.orderDetails.payPrice += item.rolePrice * item.goodsQuantity
      this.orderDetails.payAmount += item.goodsPrice * item.goodsQuantity
      this.orderDetails.payPrice = this.orderDetails.payPrice.toFixed(2)
      this.orderDetails.payAmount = this.orderDetails.payAmount.toFixed(2)
    })
    this.orderDetails.payDiscount = (this.orderDetails.payAmount - this.orderDetails.payPrice)
    console.log(this.orderDetails)
  }
}
</script>

<style lang="less">
page{
  background-color: #f6f6f6;
}
.topay-container{
  margin-top: 18rpx;
}
.flex-row{
  display: flex;
  flex-direction: row;
}
.flex-column{
  display: flex;
  flex-direction: column;
}
.white-box{
  width: 750rpx;
  box-sizing: border-box;
  background-color: #fff;
  margin-bottom: 16rpx;
  padding: 32rpx;
}
.topay-address{
  justify-content: space-between;
  align-items: center;
  .address-content{
    .receiver-name{
      .receiver-icon{
        width: 50rpx;
        height: 50rpx;
        background:linear-gradient(137deg,rgba(249,182,38,1) 0%,rgba(255,111,20,1) 100%);
        box-shadow:0px 2px 12px rgba(254,123,23,0.4);
        border-radius: 50%;
        color: #fff;
        font-size: 20rpx;
        display: flex;
        justify-content: center;
        align-items: center;
        margin-right: 16rpx;
      }
      .name-text{
        color: #333;
        font-size: 32rpx;
      }
    }
    .address-selected{
      width: 452rpx;
      margin-left: 66rpx;
      .address-community{
        color: #999;
        font-size: 24rpx;
        line-height: 34rpx;
      }
      .address-text{
        color: #111;
        font-size: 28rpx;
        line-height: 40rpx;
      }
    }
  }
  .arrow-right-icon{
    width: 20rpx;
    height: 32rpx;
  }
}
.topay-goods{
  padding: 0 32rpx;
  .goods-number-btn{
    width: 48rpx;
    height: 48rpx;
  }
  .goods-cell{
    height: 256rpx;
    align-items: center;
    border-bottom: 2rpx solid #f5f5f5;
    .cell-img{
      width: 180rpx;
      height: 180rpx;
      background-color: grey;
      margin-right: 40rpx;
    }
    .cell-content{
      height: 180rpx;
      width: 466rpx;
      justify-content: space-between;
      .goods-name{
        font-size: 28rpx;
        color: #111;
        line-height: 40rpx;
        padding-bottom: 10rpx;
      }
      .goods-specifications{
        color: #999;
        font-size: 22rpx;
      }
      .goods-bottom{
        justify-content: space-between;
        align-items: center;
        .goods-price{
          align-items: flex-end;
          .pay-num{
            color: #111;
            font-size: 36rpx;
          }
          .origi-num{
            color: #666;
            font-size: 24rpx;
            padding-bottom: 6rpx;
            text-decoration: line-through;
            margin-left: 10rpx;
          }
        }
        .goods-opera{
          align-items: center;
          .goods-number{
            color: #666;
            font-size: 28rpx;
            padding: 0 20rpx;
          }
        }
      }
    }
  }
}
.topay-delivery{
  .delivery-radio{
    color: #111;
    font-size: 32rpx;
    margin-right: 115rpx;
    radio{
      transform: scale(0.75);
    }
  }
}
.topay-discont{
  .discount-cell{
    justify-content: space-between;
    align-items: center;
    height: 100rpx;
    .cell-left{
      .diacount-title{
        color: #111;
        font-size: 32rpx;
      }
    }
    .cell-right{
      color: #666;
      font-size: 24rpx;
      align-items: center;
    }
    .arrow-right-icon{
      width: 48rpx;
      height: 48rpx;
    }
  }
}
.bottom-space{
  height: 182rpx;
  width: 750rpx;
}
.topay-bottom{
  justify-content: space-between;
  align-items: center;
  position: fixed;
  height: 182rpx;
  bottom: 0;
  left: 0;
  margin-bottom: 0;
  border-radius:40px 40px 0px 0px;
  padding: 0 52rpx;
  box-shadow:0px -20px 40px rgba(214,220,228,0.4);
  .bottom-left{
    .total-text{
      color: #111;
      font-size: 20rpx;
    }
    .pay-amount{
      color: #011706;
      font-size: 48rpx;
      font-weight: bold;
    }
    .pay-detail{
      color: #727C8E;
      font-size: 24rpx;
    }
  }
  .topay-btn{
    width: 300rpx;
    color: #fff;
    height: 100rpx;
    line-height: 100rpx;
    text-align: center;
    border-radius: 100rpx;
    background-color: #20c442;
    box-shadow: 4rpx 8rpx 20rpx rgba(32, 196, 66, 0.5);
    font-size: 32rpx;
  }
}
</style>
