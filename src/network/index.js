const serve = 'http://47.106.133.124:8088'
// const serve = 'http://192.168.0.145:8088'

const getToken = () => {
  return wx.getStorageSync('token')
}

const http = (url, type, params) => {
  let config = {
    url: `${serve}${url}`,
    method: type,
    header: { 'token': getToken() }
        // header: { 'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjoie1wiY3JlYXRlVGltZVwiOjE1NjAzOTYzOTIwMDAsXCJnZW5kZXJcIjpcIuS_neWvhlwiLFwiaGVhZFBvcnRyYWl0XCI6XCLpu5jorqTlpLTlg49cIixcImlzRGlzYWJsZWRcIjpmYWxzZSxcImlzR3JvdXBMZWFkZXJcIjpmYWxzZSxcImlzVmlwXCI6ZmFsc2UsXCJsb2dpblRpbWVcIjoxNTYyMTY0OTU3NTc5LFwibmlja25hbWVcIjpcIjE1Mzc4MTk1MzQwXCIsXCJwaG9uZU51bWJlclwiOlwiMTUzNzgxOTUzNDBcIixcInVzZXJJZFwiOjY1MDkxMjIzNjI5MDE1MDQsXCJ1c2VyUm9sZUlkXCI6NjQ4NDEzNDkxMDM5NTM5Mn0ifQ.9BO8iHToRlm8mXsRBo1BdTfC3MjJ9nIst7EyVU-0rNM' }
  }
  if (params) config.data = params
  return new Promise((resolve, reject) => {
    wx.request({
      ...config,
      success: (res) => {
        resolve(res.data)
      },
      fail: (err) => {
        reject(err)
      }
    })
  }).catch((error) => {})
}
export { http }
