import { http } from './index'

export function loginByPhoneNumber(phoneNumber, valCode) {
    return http(`/app/user/loginByPhoneNumber/${phoneNumber}/${valCode}`, 'GET')
}

export function loginByWechat(querys) {
    return http(`/app/user/loginByWechat${querys}`, 'GET')
}

// 发送手机验证码（绑定 SMS_167533669；登录 SMS_164095544）
export function sendSMS(phoneNumber, templateCode) {
    return http(`/app/user/sendSMS/${phoneNumber}/${templateCode}`, 'GET')
}

export function bindPhoneNumber(phoneNumber, valCode, code, userName, photo, gender) {
    return http(`/app/user/bindPhoneNumber/${phoneNumber}/${valCode}?code=${code}&wechatNickname=${userName}&wechatHeadPortrait=${photo}&gender=${gender}`, 'PUT')
}

export function getUserInfoById(userId) {
    return http(`/app/user/${userId}`, 'GET')
}

export function getBannerAd(params) {
    return http(`/app/bannerAd/selectList`, 'POST', params)
}
// 获取商品分类列表
export function getGoodsClassify() {
    return http(`/app/goodsClassify/list`, 'POST')
}

export function getGoodsList(params) {
    return http(`/app/goods/list`, 'POST', params)
}

// 获取团长列表
export function getLeaderList(params) {
    return http(`/app/groupLeaderInfo/selectList`, 'POST', params)
}
// 根据关键字获取团长列表
export function getLeaderListBykeyword(params) {
    return http(`/app/groupLeaderInfo/selectListByKeyword`, 'POST', params)
}
// 根据id获取团长信息
export function getLeaderInfo(groupLeaderInfoId) {
    return http(`/app/groupLeaderInfo/${groupLeaderInfoId}`, 'GET', {})
}

export function userChoiceLeader(params) {
    return http(`/app/userChoiceLeader`, 'POST', params)
}

export function addToCart(params) {
    return http(`/app/cart`, 'POST', params)
}

export function modifyCart(params) {
    return http(`/app/cart`, 'PUT', params)
}

export function getGoodsDetail(goodsId, warehouseId) {
    return http(`/app/goods/${goodsId}/${warehouseId}`, 'GET')
}

export function getCartList(params) {
    return http(`/app/cart/selectList`, 'POST', params)
}

export function deleteCart(cartIds, userId) {
    return http(`/app/cart/${cartIds}/${userId}`, 'DELETE')
}

// 获取优惠券的列表
export function getCouponList(params) {
    return http(`/app/userDiscountCoupon/selectList`, 'POST', params)
}

// 获取最优优惠券的列表
export function getMostCoupon(params) {
    return http(`/app/userDiscountCoupon/selectUsable`, 'POST', params)
}
// 获取领券中心的列表
export function getCouponCenterList(params) {
    return http(`/app/discountCoupon/selectList`, 'POST', params)
}
// 关键字联想
export function inputKeyword(params) {
    return http(`/app/search/reverie/${params}`, 'GET', {})
}
// 搜索热门推荐
export function searchHotList(params) {
    return http(`/app/search/hotGoods/${params}`, 'GET', {})
}
// 搜索新鲜速递
export function searchNewGoods(warehouseId) {
    return http(`/app/search/nowGoods/${warehouseId}`, 'GET', {})
}
// 搜索历史
export function searchHistory(params) {
    return http(`/app/search/userSearch`, 'GET', {})
}
// 获取省市区（收货地址）
export function getAreaList(parentId) {
    return http(`/app/administrativeDivision/${parentId}`, 'GET', {})
}
// 添加收货地址
export function addAddress(params) {
    return http(`/app/userDeliveryAddress`, 'POST', params)
}
// 获取地址列表
export function getAddressList(pageNum, pageSize) {
    return http(`/app/userDeliveryAddress/selectList/${pageNum}/${pageSize}`, 'GET', {})
}
// 获取地址详情
export function getAddressDetail(userDeliveryAddressId) {
    return http(`/app/userDeliveryAddress/${userDeliveryAddressId}`, 'GET', {})
}
// 修改地址
export function updateAddress(params) {
    return http(`/app/userDeliveryAddress`, 'PUT', params)
}
// 查询默认地址
export function getDefaultAddress() {
    return http(`/app/userDeliveryAddress/selectDefault`, 'GET', {})
}
// 创建订单
export function createOrder(params) {
    return http(`/app/order`, 'POST', params)
}
// 支付下单
export function payment(params) {
    return http(`/app/wechatPay/appletUnifiedOrder`, 'POST', params)
}
// 获取评论列表
export function getCommentList(params) {
    return http(`/app/goodsComment/list`, 'POST', params)
}