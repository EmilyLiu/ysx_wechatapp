import { http } from './index'

// 修改用户的基本信息
export function updateUserMsg(params) {
  return http(`/app/user`, 'PUT', params)
}
// 获取当前用户的订单列表
export function getOrderList(params) {
  return http(`/app/order/list`, 'POST', params)
}
// 绑定邮箱
export function bindEmail(userId, emailAddress) {
  return http(`/app/user/bindEamil/${userId}/${emailAddress}`, 'PUT', )
}
// 绑定手机号码
export function bindPhone(userId, phoneNumber, valCode) {
  return http(`/app/user/updatePhoneNumber/${userId}/${phoneNumber}/${valCode}`, 'PUT', )
}
// 获取当前用户的订单的详情（根据订单的ID）
export function getOrderDetailsById(userId) {
  return http(`/app/order/${userId}`, 'GET')
}
// 取消订单
export function cancelOrder(orderId) {
  return http(`/app/order/cancel/${orderId}`, 'PUT')
}
// 查询团长的钱包
export function getGroupLeaderWallet() {
  return http(`/app/groupLeaderWallet/groupLeaderMoneyInfo`, 'GET')
}
// 领取优惠券
export function getCoupon(params) {
  return http(`/app/userDiscountCoupon`, 'POST', params)
}
// 获取优惠券详情
export function getCouponDetails(userDiscountCouponId) {
  return http(`/app/userDiscountCoupon/${userDiscountCouponId}`, 'GET')
}
// 获取团长钱包的明细
export function getGroupLeaderWalletList(params) {
  return http(`/app/groupLeaderCashFlow/list`, 'POST', params)
}
// 获取团长订单列表
export function getGroupLeaderOrderList(params) {
  return http(`/app/groupLeaderOrder/list`, 'POST', params)
}
// 根据用户id,查询当前用户的团长id
export function getcurUserGroupLeaderIdByUserId() {
  return http(`/app/groupLeaderInfo/selectByUserId`, 'GET')
}
// 团长管理（提醒收货， 到货提醒）
export function reminderArrival(orderId) {
  return http(`/app/groupLeaderOrder/remind/${orderId}`, 'GET')
}
// 团长管理（查看订单的详情）
export function groupLeaderGetDetails(orderId) {
  return http(`/app/groupLeaderOrder/${orderId}`, 'GET')
}
// 申请团长
export function applyLeader(params) {
  return http(`/app/groupLeaderInfo`, 'POST', params)
}
// 获取团长申请状态
export function leaderStatus() {
  return http(`/app/groupLeaderInfo/selectByUserId`, 'GET')
}
// 根据用户id获取提货的订单
export function getOrderByUserId(userId) {
  return http(`/app/groupLeaderOrder/getByUserId/${userId}`, 'GET')
}
// 根据提货码查询订单
export function getOrderByCode(code) {
  return http(`/app/groupLeaderOrder/getByTackCode/${code}`, 'GET')
}
// 确认提货
export function confirmPick(orderId) {
  return http(`/app/groupLeaderOrder/completed/${orderId}`, 'PUT')
}
// 添加商品评论
export function addComment(params) {
  return http(`/app/goodsComment`, 'POST', params)
}
// 获取商品评论列表
export function getCommentList(params) {
  return http(`/app/goodsComment/list`, 'POST', params)
}
