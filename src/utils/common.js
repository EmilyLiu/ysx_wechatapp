export function paramsToQuery(obj) {
  let query = '?'
  let keysArr = Object.keys(obj)
  let len = keysArr.length
  keysArr.forEach((item, index) => {
    query += `${item}=${obj[item]}`
    if (index < len - 1) query += '&'
  })
  return query
}
