import wepy from 'wepy'
function tipsError (msg) {
  wepy.showModal({
    title: '温馨提示',
    content: msg,
    showCancel: false,
    confirmText: '我知道了',
    success: function(res) {
    }
  })
}
function reLogin () {
  wepy.showModal({
    title: '温馨提示',
    content: '您的登录信息已过期，请重新登录',
    showCancel: false,
    confirmText: '立即登录',
    success: function(res) {
      console.log(res)
      if (res.confirm) {
        wepy.redirectTo({ url: '/pages/user/authorization' })
      }
    }
  })
}
module.exports = {
  tipsError: tipsError,
  reLogin: reLogin
}