/**
 * 手机号码隐藏中间四位
 * @param {*} telNum 电话号码
 */
export function hiddenPhone(telNum) {
    var tel = telNum
    var reg = /^(\d{3})\d{4}(\d{4})$/
    tel = tel.replace(reg, '$1****$2')
    return tel;
}