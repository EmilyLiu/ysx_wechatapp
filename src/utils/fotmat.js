const formatTimeString = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month].map(formatNumberString).join('年') + '月' + day + '日'
}
const formatTime = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hour = date.getHours()
    const minute = date.getMinutes()
    const second = date.getSeconds()
  
    return [year, month].map(formatNumber).join('-')
}
const formatNumberString = n => {
  n = n.toString()
  return n[1] ? n : n
}
const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}
const formatTimerCount = (time) => {
  let days = Math.floor(time / (24 * 3600 * 1000))
  let subTime1 = time%(24*3600*1000)
  let hours = Math.floor(subTime1/(3600*1000))
  let subTime2 = subTime1%(3600*1000)
  let minutes = Math.floor(subTime2/(60*1000))
  let subTime3 =subTime2%(60*1000)
  let seconds = Math.round(subTime3/1000)
  if (hours < 10) {
      hours = '0' + hours
  }
  if (minutes < 10) {
    minutes = '0' + minutes
  }
  if (seconds < 10) {
    seconds = '0' + seconds
  }
  let timeObj = {
    days,
    hours,
    minutes,
    seconds
  }
  return timeObj
}
module.exports = {
  formatTimeString: formatTimeString,
  formatTime: formatTime,
  formatTimerCount: formatTimerCount,
  formatTimerCount
}
  