<style lang='less'>
page {
  font-family: PingFang SC;
}
.container {
  height: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  box-sizing: border-box;
}
</style>

<script>
import wepy from 'wepy'
import 'wepy-async-function'

import { setStore } from 'wepy-redux'
import configStore from './store'
import { paramsToQuery } from './utils/common.js'
import { loginByWechat, getUserInfoById } from './network/api'
import error from './utils/error.js'

const store = configStore()
setStore(store)

export default class extends wepy.app {
  config = {
    pages: [
      'pages/shop/index',
      'pages/user/goods-comment',
      'pages/user/order-comment',
      'pages/user/scavenging',
      'pages/user/pickUp-detail',
      'pages/user/pickup',
      'pages/user/select-leader',
      'pages/index',
      'pages/user/mine',
      'pages/shop/qrcode',
      'pages/shop/buyer-more',
      'pages/shop/search-keyword',
      'pages/shop/search_index',
      'pages/user/address',
      'pages/user/address-edit',
      'pages/user/address-search',
      'pages/shop/buyer-team',
      'pages/shop/search',
      'pages/shop/goods-detail',
      'pages/user/authorization',
      'pages/user/cart',
      'pages/user/login',
      'pages/user/coupon',
      'pages/user/voucher-center',
      'pages/user/coupon-detail',
      'pages/user/order',
      'pages/user/order-detail',
      'pages/user/refund-apply',
      'pages/user/source',
      'pages/user/system',
      'pages/user/user-info',
      'pages/user/leader-apply',
      'pages/user/phone-modify',
      // 'pages/user/phone-bind',
      'pages/user/email-modify',
      'pages/user/order-topay',
      'pages/user/coupon-select',
      'pages/user/update-nikename',
      'pages/user/update-birth',
      'pages/user/groupMange',
      'pages/user/groupLeaderDetails',
      'pages/user/coupon-explain',
      'pages/user/walletDetails',
      'pages/user/common-problem',
      'pages/user/problemDetails'
    ],
    permission: {
      'scope.userLocation': {
        desc: '你的位置信息将用于小程序位置接口的效果展示'
      }
    },
    window: {
      backgroundTextStyle: 'light',
      navigationBarBackgroundColor: '#fff',
      navigationBarTitleText: 'WeChat',
      navigationBarTextStyle: 'black'
    },
    tabBar: {
      color: '#9b9b9b',
      selectedColor: '#1C202D',
      list: [
        {
          pagePath: 'pages/shop/index',
          iconPath: '/images/index_default.png',
          selectedIconPath: '/images/index_active.png',
          text: '首页'
        },
        {
          pagePath: 'pages/user/cart',
          iconPath: '/images/cart_default.png',
          selectedIconPath: '/images/cart_active.png',
          text: '购物车'
        },
        {
          pagePath: 'pages/user/mine',
          iconPath: '/images/mine_default.png',
          selectedIconPath: '/images/mine_active.png',
          text: '我的'
        }
      ]
    }
  }

  globalData = {
    userInfo: null
  }

  constructor() {
    super()
    this.use('requestfix')
  }

  onLaunch() {
    this.testAsync()
    const that = this
    wepy.getSetting({
      success: res => {
        let auth = res.authSetting
        let token = wepy.getStorageSync('token')
        console.log(token)
        let userId = wepy.getStorageSync('userId')
        let warehouseId = wepy.getStorageSync('warehouseId')
        let groupLeaderInfoId = wepy.getStorageSync('groupLeaderInfoId')
        if (auth['scope.userInfo'] && token && userId && warehouseId && groupLeaderInfoId) {
          // that.login()

        } else {
          wepy.redirectTo({ url: '/pages/user/authorization' })
        }
      }
    })
  }
  login() {
    const that = this
    wepy.getUserInfo({
      success(res) {
        let userInfo = res.userInfo
        wepy.login({
          success: res => {
            let query = {
              code: res.code,
              wechatNickname: userInfo.nickName,
              wechatHeadPortrait: userInfo.avatarUrl,
              gender:
                userInfo.gender === 1
                  ? '男'
                  : userInfo.gender === 2
                    ? '女'
                    : '保密'
            }
            query = paramsToQuery(query)
            loginByWechat(query).then(res => {
              if (res.code === 200) {
                wepy.setStorageSync('userId', res.data.loginObject.userId)
                wepy.setStorageSync('token', res.data.token)
                if (!res.data.loginObject.phoneNumber) {
                  wepy.redirectTo({ url: '/pages/user/login' })
                } else {
                  that.globalData.userInfo = res.data.loginObject
                  let groupLeaderInfoId = wepy.getStorageSync(
                    'groupLeaderInfoId'
                  )
                  if (groupLeaderInfoId) {
                    wepy.redirectTo({ url: '/pages/shop/index' })
                  } else {
                    wepy.redirectTo({ url: '/pages/user/select-leader' })
                  }
                }
              } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
                error.reLogin()
              } else {
                error.tipsError(res.msg)
              }
            })
          },
          fail: () => {},
          complete: () => {}
        })
      }
    })
  }

  sleep(s) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve('promise resolved')
      }, s * 1000)
    })
  }

  async testAsync() {
    const data = await this.sleep(3)
  }

  getUserInfo(callback) {
    if (this.globalData.userInfo) {
      callback(this.globalData.userInfo)
    }
    const userId = wepy.getStorageSync('userId')
    getUserInfoById(userId).then(res => {
      if (res.code === 200) {
        this.globalData.userInfo = res.data
        callback(res.data)
      } else if (res.code === 1000 || res.code === 1001 || res.code === 16) {
        error.reLogin()
      } else {
        error.tipsError(res.msg)
      }
    })
  }
}
</script>

<style lang="less">
.text_ellipsis {
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
}
.flex-row {
  display: flex;
  flex-direction: row;
  // flex-wrap:wrap;
}
.flex-column {
  display: flex;
  flex-direction: column;
}
.flex_between {
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    flex-wrap: wrap;
}
.flex_center {
  display: flex;
  flex-direction: row;
  justify-content: center;
  flex-wrap: wrap;
}
.flex{
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
}
</style>
